from app import __metadata__ as meta

def getMode():
    with open('./logs/{}.log'.format(meta.APP_NAME.lower()), 'r') as f:
        for line in f.readlines():
            if line.lower().count('mode is:'):
                x = line.lower().split('mode is:')
                x = x[1].split('\n')[0]
                return x